package posmy.interview.boot.common;

public class CustomDetailResponse {
	private String field;
	private Object message;
	
	public CustomDetailResponse(String field, Object message) {
		this.field = field;
		this.message = message;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Object getMessage() {
		return message;
	}

	public void setMessage(Object message) {
		this.message = message;
	}
}
