package posmy.interview.boot.common;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class CommonUtil {
	
	public boolean checkUserAuthorization(String username, Authentication authentication) {
		boolean authorized = false;
		
		if (username != null && !username.equalsIgnoreCase(authentication.getName())) {
			for (GrantedAuthority authority : authentication.getAuthorities()) {
				if ("ROLE_LIBRARIAN".equalsIgnoreCase(authority.getAuthority())) {
					authorized = true;
					break;
				}
			}
		} else if (username != null && username.equalsIgnoreCase(authentication.getName())) {
			authorized = true;
		}
		
		return authorized;
	}
	
	public boolean checkAdminAuthorization(Authentication authentication) {
		boolean authorized = false;
		
		for (GrantedAuthority authority : authentication.getAuthorities()) {
			if ("ROLE_LIBRARIAN".equalsIgnoreCase(authority.getAuthority())) {
				authorized = true;
				break;
			}
		}
		
		return authorized;
	}

}
