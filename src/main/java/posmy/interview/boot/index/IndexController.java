package posmy.interview.boot.index;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> indexPage(Authentication authentication) {
		if (authentication.getName() != null)
			return ResponseEntity.ok("You are now logged in as " + authentication.getName());
		else
			return new ResponseEntity<>("", HttpStatus.UNAUTHORIZED);
	}

}
