package posmy.interview.boot.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO extends JpaRepository<User, Integer> {
	User findByUsername(String username);
	void deleteByUsername(String username);

}
