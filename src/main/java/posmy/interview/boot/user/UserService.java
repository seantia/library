package posmy.interview.boot.user;

public interface UserService {
	public abstract User retrieveUserInfoByUsername(String username);
	public abstract User create(User user);
	public abstract User update(User user);
	public abstract void deleteByUsername(String username);

}
