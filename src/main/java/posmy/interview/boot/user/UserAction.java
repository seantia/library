package posmy.interview.boot.user;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import posmy.interview.boot.common.CommonUtil;
import posmy.interview.boot.common.CustomDetailResponse;
import posmy.interview.boot.common.CustomResponse;

@Component
public class UserAction {
	private UserService userService;
	private BCryptPasswordEncoder bcryptEncoder;
	
	public UserAction(UserService userService, BCryptPasswordEncoder bcryptEncoder) {
		this.userService = userService;
		this.bcryptEncoder = bcryptEncoder;
	}
	
	/**
	 * Create new user in database.
	 * Retrieve the new user to ensure user creation is successful.
	 * 
	 * @param user
	 * @return custom response
	 */
	public CustomResponse createNewUserAccount(UserDTO user) {
		User newUser = new User();
		User existingUser = new User();
		CustomResponse response = new CustomResponse();
		ArrayList<CustomDetailResponse> responseDetailList = new ArrayList<>();
		
		try {
			existingUser = userService.retrieveUserInfoByUsername(user.getUsername());
			
			if (existingUser == null) {
				newUser.setUsername(user.getUsername());
				newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
				newUser.setRole(user.getRole());
				newUser.setEnabled(user.isEnabled());
				
				newUser = userService.create(newUser);
				
				if (newUser != null) {
					if (newUser.getId() != 0) {
						response.setStatus("success");
						responseDetailList.add(new CustomDetailResponse("", "User created successfully"));
						response.setDetails(responseDetailList);
					} else {
						response.setStatus("failure");
						responseDetailList.add(new CustomDetailResponse("", "User creation failed"));
						response.setDetails(responseDetailList);
					}
				} else {
					response.setStatus("failure");
					responseDetailList.add(new CustomDetailResponse("", "User creation failed"));
					response.setDetails(responseDetailList);
				}
			} else {
				response.setStatus("failure");
				responseDetailList.add(new CustomDetailResponse("", "Username taken"));
				response.setDetails(responseDetailList);
			}
		} catch(AccessDeniedException e) {
			response.setStatus("unauthorized");
			responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
			response.setDetails(responseDetailList);
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("failure");
			responseDetailList.add(new CustomDetailResponse("", "Error encountered. Please consult support"));
			response.setDetails(responseDetailList);
		}
		
		return response;
	}
	
	/**
	 * 
	 * Get user information from database
	 * 
	 * @param username
	 * @param authentication
	 * @return custom response
	 */
	public CustomResponse retrieveUserInfo(String username, Authentication authentication) {
		CommonUtil util = new CommonUtil();
		User retrievedUser = new User();
		CustomResponse response = new CustomResponse();
		ArrayList<CustomDetailResponse> responseDetailList = new ArrayList<>();
		
		try {
			if (util.checkUserAuthorization(username, authentication)) { 
				retrievedUser = userService.retrieveUserInfoByUsername(username);
			}  else {
				response.setStatus("unauthorized");
				responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
				response.setDetails(responseDetailList);
				
				return response;
			}
			
			if (retrievedUser != null) {
				if (retrievedUser.getId() != 0) {
					LinkedHashMap<String, Object> detailsMap = new LinkedHashMap<String, Object>();
					detailsMap.put("id", retrievedUser.getId());
					detailsMap.put("name", retrievedUser.getUsername());
					detailsMap.put("role", retrievedUser.getRole());
					detailsMap.put("enabled", retrievedUser.isEnabled());
					
					response.setStatus("success");
					responseDetailList.add(new CustomDetailResponse("", detailsMap));
					response.setDetails(responseDetailList);
				} else {
					response.setStatus("failure");
					responseDetailList.add(new CustomDetailResponse("", "User not found"));
					response.setDetails(responseDetailList);
				}
			} else {
				response.setStatus("failure");
				responseDetailList.add(new CustomDetailResponse("", "User not found"));
				response.setDetails(responseDetailList);
			}
		} catch(AccessDeniedException e) {
			response.setStatus("unauthorized");
			responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
			response.setDetails(responseDetailList);
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("failure");
			responseDetailList.add(new CustomDetailResponse("", "Error encountered. Please consult support"));
			response.setDetails(responseDetailList);
		}
		
		return response;
	}
	
	/**
	 * 
	 * Update user information by retrieving the original records in database
	 * 
	 * @param username
	 * @param user
	 * @param authentication
	 * @return custom response
	 */
	public CustomResponse updateUserInfo(String username, UserDTO user, Authentication authentication) {
		CommonUtil util = new CommonUtil();
		User userInfo = new User();
		CustomResponse response = new CustomResponse();
		ArrayList<CustomDetailResponse> responseDetailList = new ArrayList<>();
		
		try {
			// Get original user information
			if (util.checkUserAuthorization(username, authentication)) {
				userInfo = userService.retrieveUserInfoByUsername(username);
			} else {
				response.setStatus("unauthorized");
				responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
				response.setDetails(responseDetailList);
				
				return response;
			}
			
			if (!"".equalsIgnoreCase(user.getUsername()) && user.getUsername() != null)
				userInfo.setUsername(user.getUsername());
			
			if (!"".equalsIgnoreCase(user.getPassword()) && user.getPassword() != null)
				userInfo.setPassword(bcryptEncoder.encode(user.getPassword()));
			
			if (!"".equalsIgnoreCase(user.getRole()) && user.getRole() != null)
				userInfo.setRole(user.getRole());
			
			if (Boolean.compare(user.isEnabled(), userInfo.isEnabled()) != 0)
				userInfo.setEnabled(user.isEnabled());
			
			userService.update(userInfo);
			
			response.setStatus("success");
			responseDetailList.add(new CustomDetailResponse("", "User updated successfully"));
			response.setDetails(responseDetailList);
			
		} catch(AccessDeniedException e) {
			response.setStatus("unauthorized");
			responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
			response.setDetails(responseDetailList);
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("failure");
			responseDetailList.add(new CustomDetailResponse("", "Error encountered. Please consult support"));
			response.setDetails(responseDetailList);
		}
		
		return response;
	}
	
	/**
	 * 
	 * Delete user account based on username
	 * 
	 * @param username
	 * @param authentication
	 * @return
	 */
	public CustomResponse deleteUserAccount(String username, Authentication authentication) {
		CommonUtil util = new CommonUtil();
		User retrievedUser = new User();
		CustomResponse response = new CustomResponse();
		ArrayList<CustomDetailResponse> responseDetailList = new ArrayList<>();
		
		try {
			if (util.checkUserAuthorization(username, authentication)) {
				userService.deleteByUsername(username);
				retrievedUser = userService.retrieveUserInfoByUsername(username);
			}  else {
				response.setStatus("unauthorized");
				responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
				response.setDetails(responseDetailList);
				
				return response;
			}
			
			if (retrievedUser == null) {
				response.setStatus("success");
				responseDetailList.add(new CustomDetailResponse("", "User deleted successfully"));
				response.setDetails(responseDetailList);
			} else {
				response.setStatus("failure");
				responseDetailList.add(new CustomDetailResponse("", "User deletion failed"));
				response.setDetails(responseDetailList);
			}
		} catch(AccessDeniedException e) {
			response.setStatus("unauthorized");
			responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
			response.setDetails(responseDetailList);
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("failure");
			responseDetailList.add(new CustomDetailResponse("", "Error encountered. Please consult support"));
			response.setDetails(responseDetailList);
		}
		
		return response;
	}

}
