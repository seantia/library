package posmy.interview.boot.user;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
	private UserDAO userDAO;

	public UserServiceImpl(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_MEMBER')")
	@Override
	public User retrieveUserInfoByUsername(String username) {

		return userDAO.findByUsername(username);
	}

	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	@Override
	public User create(User user) {

		return userDAO.save(user);
	}

	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	@Override
	public User update(User user) {

		return userDAO.save(user);
	}

	@Transactional
	@PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_MEMBER')")
	@Override
	public void deleteByUsername(String username) {

		userDAO.deleteByUsername(username);
	}

}
