package posmy.interview.boot.user;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import posmy.interview.boot.common.CustomResponse;

@RestController
@RequestMapping(value = "/users")
public class UserController {
	private UserAction userAction;
	
	public UserController(UserAction userAction) {
		this.userAction = userAction;
	}
	
	/**
	 * Get specific user based on username
	 * 
	 * @return response
	 */
	@RequestMapping(value = "/{username}", method = RequestMethod.GET)
	public ResponseEntity<?> retrieveUser(@PathVariable String username, Authentication authentication) {
		CustomResponse response = userAction.retrieveUserInfo(username, authentication);
		
		if ("success".equalsIgnoreCase(response.getStatus()))
			return ResponseEntity.ok(response);
		else if ("unauthorized".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		else
			return ResponseEntity.badRequest().body(response);
	}
	
	/**
	 * Create user by consuming JSON that contains the details
	 * 
	 * @return response
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> createNewUser(@RequestBody UserDTO user) {
		CustomResponse response = userAction.createNewUserAccount(user);
		
		if ("success".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.CREATED);
		else if ("unauthorized".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		else
			return ResponseEntity.badRequest().body(response);
	}
	
	/**
	 * Update user by consuming JSON that contains the details
	 * 
	 * @return response
	 */
	@RequestMapping(value = "/{username}", method = RequestMethod.PATCH)
	public ResponseEntity<?> updateUser(@PathVariable String username, @RequestBody UserDTO user, Authentication authentication) {
		CustomResponse response = userAction.updateUserInfo(username, user, authentication);
		
		if ("success".equalsIgnoreCase(response.getStatus()))
			return ResponseEntity.ok(response);
		else if ("unauthorized".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		else
			return ResponseEntity.badRequest().body(response);
	}
	
	/**
	 * Delete user based on username
	 * 
	 * @return response
	 */
	@RequestMapping(value = "/{username}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable String username, Authentication authentication) {
		CustomResponse response = userAction.deleteUserAccount(username, authentication);
		
		if ("success".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
		else if ("unauthorized".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		else
			return ResponseEntity.badRequest().body(response);
	}

}
