package posmy.interview.boot.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
	private DataSource dataSource;
	
	public WebSecurityConfiguration(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
    
    @Autowired
	void configAuthentication(AuthenticationManagerBuilder authBuilder) throws Exception {
    	authBuilder.jdbcAuthentication()
    		.passwordEncoder(new BCryptPasswordEncoder())
    		.dataSource(dataSource)
    		.usersByUsernameQuery("select username, password, enabled from users where username=?")
    		.authoritiesByUsernameQuery("select username, role from users where username=?");
    }
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.csrf().disable()
        	.authorizeRequests()
        	.antMatchers("/").access("hasRole('LIBRARIAN') or hasRole('MEMBER')")
        	.antMatchers("/users/**").access("hasRole('LIBRARIAN') or hasRole('MEMBER')")
        	.antMatchers("/books/**").access("hasRole('LIBRARIAN') or hasRole('MEMBER')")
            .anyRequest().authenticated()
            .and()
			.formLogin().permitAll()
			.and()
			.logout().permitAll()
			.and()
			.exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint());
    }

}
