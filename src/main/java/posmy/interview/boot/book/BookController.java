package posmy.interview.boot.book;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import posmy.interview.boot.common.CustomResponse;

@RestController
@RequestMapping(value = "/books")
public class BookController {
	private BookAction bookAction;
	
	public BookController(BookAction bookAction) {
		this.bookAction = bookAction;
	}
	
	/**
	 * 
	 * Get book information based on ID
	 * 
	 * @param id
	 * @return response
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> retrieveBookInfo(@PathVariable int id) {
		CustomResponse response = bookAction.retrieveBookInfo(id);
		
		if ("success".equalsIgnoreCase(response.getStatus()))
			return ResponseEntity.ok(response);
		else if ("unauthorized".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		else
			return ResponseEntity.badRequest().body(response);
	}
	
	/**
	 * 
	 * Create new book by consuming JSON that contains the details
	 * 
	 * @param book
	 * @return response
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> createNewBook(@RequestBody BookDTO book) {
		CustomResponse response = bookAction.createNewBook(book);
		
		if ("success".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.CREATED);
		else if ("unauthorized".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		else
			return ResponseEntity.badRequest().body(response);
	}
	
	/**
	 * 
	 * Update book information based on ID and JSON that contains the details
	 * 
	 * @param id
	 * @param book
	 * @param authentication
	 * @return response
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public ResponseEntity<?> updateBook(@PathVariable int id, @RequestBody BookDTO book, Authentication authentication) {
		CustomResponse response = bookAction.updateBookInfo(id, book, authentication);
		
		if ("success".equalsIgnoreCase(response.getStatus()))
			return ResponseEntity.ok(response);
		else if ("unauthorized".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		else
			return ResponseEntity.badRequest().body(response);
	}
	
	/**
	 * 
	 * Delete book based on ID
	 * 
	 * @param id
	 * @return response
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteBook(@PathVariable int id) {
		CustomResponse response = bookAction.deleteBook(id);
		
		if ("success".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
		else if ("unauthorized".equalsIgnoreCase(response.getStatus()))
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		else
			return ResponseEntity.badRequest().body(response);
	}

}
