package posmy.interview.boot.book;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BookDAO extends JpaRepository<Book, Integer> {
	Book findById(int id);
	void deleteById(int id);

}
