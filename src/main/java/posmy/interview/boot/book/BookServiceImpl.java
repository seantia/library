package posmy.interview.boot.book;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {
	private BookDAO bookDAO;
	
	public BookServiceImpl(BookDAO bookDAO) {
		this.bookDAO = bookDAO;
	}

	@PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_MEMBER')")
	@Override
	public Book retrieveBookInfoById(int id) {
		
		return bookDAO.findById(id);
	}

	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	@Override
	public Book create(Book book) {
		
		return bookDAO.save(book);
	}

	@PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_MEMBER')")
	@Override
	public Book update(Book book) {
		
		return bookDAO.save(book);
	}

	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	@Override
	public void deleteById(int id) {
		
		bookDAO.deleteById(id);
	}

}
