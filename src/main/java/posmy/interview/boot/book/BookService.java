package posmy.interview.boot.book;

public interface BookService {
	public abstract Book retrieveBookInfoById(int id);
	public abstract Book create(Book book);
	public abstract Book update(Book book);
	public abstract void deleteById(int id);

}
