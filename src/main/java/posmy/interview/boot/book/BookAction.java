package posmy.interview.boot.book;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import posmy.interview.boot.common.CommonUtil;
import posmy.interview.boot.common.CustomDetailResponse;
import posmy.interview.boot.common.CustomResponse;

@Component
public class BookAction {
	private BookService bookService;
	
	public BookAction(BookService bookService) {
		this.bookService = bookService;
	}
	
	/**
	 * 
	 * Create new book and insert into database
	 * 
	 * @param book
	 * @return custom response
	 */
	public CustomResponse createNewBook(BookDTO book) {
		Book newBook = new Book();
		CustomResponse response = new CustomResponse();
		ArrayList<CustomDetailResponse> responseDetailList = new ArrayList<>();
		
		try {
			newBook.setName(book.getName());
			newBook.setStatus(book.getStatus());
			
			newBook = bookService.create(newBook);
			
			if (newBook != null) {
				if (newBook.getId() != 0) {
					response.setStatus("success");
					responseDetailList.add(new CustomDetailResponse("", "Book created successfully"));
					response.setDetails(responseDetailList);
				} else {
					response.setStatus("failure");
					responseDetailList.add(new CustomDetailResponse("", "Book creation failed"));
					response.setDetails(responseDetailList);
				}
			} else {
				response.setStatus("failure");
				responseDetailList.add(new CustomDetailResponse("", "User creation failed"));
				response.setDetails(responseDetailList);
			}
			
		} catch(AccessDeniedException e) {
			response.setStatus("unauthorized");
			responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
			response.setDetails(responseDetailList);
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("failure");
			responseDetailList.add(new CustomDetailResponse("", "Error encountered. Please consult support"));
			response.setDetails(responseDetailList);
		}
		
		return response;
	}
	
	/**
	 * 
	 * Get book information based on ID
	 * 
	 * @param id
	 * @return custom response
	 */
	public CustomResponse retrieveBookInfo(int id) {
		Book retrievedBook = new Book();
		CustomResponse response = new CustomResponse();
		ArrayList<CustomDetailResponse> responseDetailList = new ArrayList<>();
		
		try {
			retrievedBook = bookService.retrieveBookInfoById(id);
			
			if (retrievedBook != null) {
				if (retrievedBook.getId() != 0) {
					LinkedHashMap<String, Object> detailsMap = new LinkedHashMap<String, Object>();
					detailsMap.put("id", retrievedBook.getId());
					detailsMap.put("name", retrievedBook.getName());
					detailsMap.put("status", retrievedBook.getStatus());
					detailsMap.put("borrowedBy", retrievedBook.getBorrowedBy());
					
					response.setStatus("success");
					responseDetailList.add(new CustomDetailResponse("", detailsMap));
					response.setDetails(responseDetailList);
				} else {
					response.setStatus("failure");
					responseDetailList.add(new CustomDetailResponse("", "Book not found"));
					response.setDetails(responseDetailList);
				}
			} else {
				response.setStatus("failure");
				responseDetailList.add(new CustomDetailResponse("", "Book not found"));
				response.setDetails(responseDetailList);
			}
		} catch(AccessDeniedException e) {
			response.setStatus("unauthorized");
			responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
			response.setDetails(responseDetailList);
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("failure");
			responseDetailList.add(new CustomDetailResponse("", "Error encountered. Please consult support"));
			response.setDetails(responseDetailList);
		}
		
		return response;
	}
	
	/**
	 * 
	 * Update book information based on ID and JSON that contains the details
	 * Book name can only be updated by ROLE_LIBRARIAN
	 * 
	 * @param id
	 * @param book
	 * @param authentication
	 * @return custom response
	 */
	public CustomResponse updateBookInfo(int id, BookDTO book, Authentication authentication) {
		CommonUtil util = new CommonUtil();
		Book bookInfo = new Book();
		CustomResponse response = new CustomResponse();
		ArrayList<CustomDetailResponse> responseDetailList = new ArrayList<>();
		
		try {
			bookInfo = bookService.retrieveBookInfoById(id);
			
			if (util.checkAdminAuthorization(authentication)) {
				if (!"".equalsIgnoreCase(book.getName()) && book.getName() !=null)
					bookInfo.setName(book.getName());
			}
			
			if (!"".equalsIgnoreCase(book.getStatus()) && book.getStatus() !=null)
				bookInfo.setStatus(book.getStatus());
			
			if (!"".equalsIgnoreCase(book.getBorrowedBy()) && book.getBorrowedBy() !=null)
				bookInfo.setBorrowedBy(book.getBorrowedBy());
			
			bookService.update(bookInfo);
			
			response.setStatus("success");
			responseDetailList.add(new CustomDetailResponse("", "Book updated successfully"));
			response.setDetails(responseDetailList);
			
		} catch(AccessDeniedException e) {
			response.setStatus("unauthorized");
			responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
			response.setDetails(responseDetailList);
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("failure");
			responseDetailList.add(new CustomDetailResponse("", "Error encountered. Please consult support"));
			response.setDetails(responseDetailList);
		}
		
		return response;
	}
	
	/**
	 * 
	 * Delete book based on ID
	 * 
	 * @param id
	 * @return custom response
	 */
	public CustomResponse deleteBook(int id) {
		Book bookInfo = new Book();
		CustomResponse response = new CustomResponse();
		ArrayList<CustomDetailResponse> responseDetailList = new ArrayList<>();
		
		try {
			bookService.deleteById(id);
			bookInfo = bookService.retrieveBookInfoById(id);
			
			if (bookInfo == null) {
				response.setStatus("success");
				responseDetailList.add(new CustomDetailResponse("", "Book deleted successfully"));
				response.setDetails(responseDetailList);
			} else {
				response.setStatus("failure");
				responseDetailList.add(new CustomDetailResponse("", "Book deletion failed"));
				response.setDetails(responseDetailList);
			}
		} catch(AccessDeniedException e) {
			response.setStatus("unauthorized");
			responseDetailList.add(new CustomDetailResponse("", "Unauthorized access"));
			response.setDetails(responseDetailList);
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setStatus("failure");
			responseDetailList.add(new CustomDetailResponse("", "Error encountered. Please consult support"));
			response.setDetails(responseDetailList);
		}
		
		return response;
	}

}
