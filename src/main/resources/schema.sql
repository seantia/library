CREATE SEQUENCE HIBERNATE_SEQUENCE START WITH 1 INCREMENT BY 1;

create table users(
	id int not null auto_increment primary key,
	username varchar(50) not null,
	password varchar(255) not null,
	enabled boolean not null default true,
	role varchar(50)
);

create table books(
	id int not null auto_increment primary key,
	name varchar(255) not null,
	status varchar(20) not null default 'AVAILABLE',
	borrowed_by varchar(50)
);