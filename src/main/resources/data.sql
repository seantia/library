insert into users (id, username, password, enabled, role) values
(hibernate_sequence.NEXTVAL, 'libadmin', '$2a$10$NLMKOmldkyl7PJdlBNHafu51IUiGETOkpC3L.5Ob5dxcRiDUw.aq6', TRUE, 'ROLE_LIBRARIAN');

insert into users (id, username, password, enabled, role) values
(hibernate_sequence.NEXTVAL, 'john', '$2a$10$NLMKOmldkyl7PJdlBNHafu51IUiGETOkpC3L.5Ob5dxcRiDUw.aq6', TRUE, 'ROLE_MEMBER');

insert into users (id, username, password, enabled, role) values
(hibernate_sequence.NEXTVAL, 'doe', '$2a$10$NLMKOmldkyl7PJdlBNHafu51IUiGETOkpC3L.5Ob5dxcRiDUw.aq6', TRUE, 'ROLE_MEMBER');

insert into users (id, username, password, enabled, role) values
(hibernate_sequence.NEXTVAL, 'foo', '$2a$10$NLMKOmldkyl7PJdlBNHafu51IUiGETOkpC3L.5Ob5dxcRiDUw.aq6', TRUE, 'ROLE_MEMBER');

insert into books (id, name, status) values
(hibernate_sequence.NEXTVAL, 'Book 1', 'AVAILABLE');

insert into books (id, name, status) values
(hibernate_sequence.NEXTVAL, 'Book 2', 'AVAILABLE');

insert into books (id, name, status) values
(hibernate_sequence.NEXTVAL, 'Book 3', 'AVAILABLE');
