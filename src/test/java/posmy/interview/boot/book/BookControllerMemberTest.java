package posmy.interview.boot.book;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class BookControllerMemberTest {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	@WithMockUser(username = "john", roles={"MEMBER"})
	public void getBookSuccessfulTest() throws Exception {
		String bookID = "6";
		
		mockMvc.perform(get("/books/" + bookID)
	            .contentType(MediaType.APPLICATION_JSON)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isOk())
	            .andExpect(jsonPath("$.status").value("success"));
	}
	
	@Test
	@WithMockUser(username = "john", roles={"MEMBER"})
	public void updateBookSuccessfulTest() throws Exception {
		String bookID = "6";
		String jsonString = "{ " 
				+ "\"status\": \"BORROWED\", " 
				+ "\"borrowedby\": \"john\" " 
				+ "}";
		
		mockMvc.perform(patch("/books/" + bookID)
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(jsonString)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isOk())
	            .andExpect(jsonPath("$.status").value("success"))
	            .andExpect(jsonPath("$.details[0].message").value("Book updated successfully"));
	}
	
	@Test
	@WithMockUser(username = "john", roles={"MEMBER"})
	public void createBookUnauthorizedTest() throws Exception {
		String jsonString = "{ " 
				+ "\"name\": \"New Book\", " 
				+ "\"status\": \"AVAILABLE\" " 
				+ "}";
		
		mockMvc.perform(post("/books")
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(jsonString)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isUnauthorized());
	}
	
	@Test
	@WithMockUser(username = "john", roles={"MEMBER"})
	public void deleteBookUnauthorizedTest() throws Exception {
		String bookID = "6";
		
		mockMvc.perform(delete("/books/" + bookID)
	            .contentType(MediaType.APPLICATION_JSON)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isUnauthorized());
	}

}
