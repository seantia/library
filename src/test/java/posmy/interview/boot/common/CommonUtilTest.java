package posmy.interview.boot.common;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class CommonUtilTest {
	CommonUtil util = new CommonUtil();
	
	@Test
	void checkUserAuthorizationLibrarianTest() {
		List<GrantedAuthority> roles = new ArrayList<>();
		roles.add(new SimpleGrantedAuthority("ROLE_LIBRARIAN"));
		
		Authentication authentication = new UsernamePasswordAuthenticationToken("libadmin", "12345", roles);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		Assertions.assertTrue(util.checkUserAuthorization("libadmin", authentication));
		Assertions.assertTrue(util.checkUserAuthorization("john", authentication));
	}
	
	@Test
	void checkUserAuthorizationMemberTest() {
		List<GrantedAuthority> roles = new ArrayList<>();
		roles.add(new SimpleGrantedAuthority("ROLE_MEMBER"));
		
		Authentication authentication = new UsernamePasswordAuthenticationToken("john", "12345", roles);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		Assertions.assertFalse(util.checkUserAuthorization("libadmin", authentication));
		Assertions.assertFalse(util.checkUserAuthorization("doe", authentication));
		Assertions.assertTrue(util.checkUserAuthorization("john", authentication));
	}
	
	@Test
	void checkAdminAuthorizationLibrarianTest() {
		List<GrantedAuthority> roles = new ArrayList<>();
		roles.add(new SimpleGrantedAuthority("ROLE_LIBRARIAN"));
		
		Authentication authentication = new UsernamePasswordAuthenticationToken("libadmin", "12345", roles);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		Assertions.assertTrue(util.checkAdminAuthorization(authentication));
	}
	
	@Test
	void checkAdminAuthorizationMemberTest() {
		List<GrantedAuthority> roles = new ArrayList<>();
		roles.add(new SimpleGrantedAuthority("ROLE_MEMBER"));
		
		Authentication authentication = new UsernamePasswordAuthenticationToken("john", "12345", roles);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		Assertions.assertFalse(util.checkAdminAuthorization(authentication));
	}

}
