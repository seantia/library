package posmy.interview.boot.user;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerMemberTest {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	@WithMockUser(username = "john", roles={"MEMBER"})
	public void getUserSuccessfulTest() throws Exception {
		String username = "john";
		
		mockMvc.perform(get("/users/" + username)
	            .contentType(MediaType.APPLICATION_JSON)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isOk())
	            .andExpect(jsonPath("$.status").value("success"));
	}
	
	@Test
	@WithMockUser(username = "foo", roles={"MEMBER"})
	public void deleteUserSuccessfulTest() throws Exception {
		String username = "foo";
		
		mockMvc.perform(delete("/users/" + username)
	            .contentType(MediaType.APPLICATION_JSON)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isNoContent());
	}
	
	@Test
	@WithMockUser(username = "john", roles={"MEMBER"})
	public void getUserUnauthorizedTest() throws Exception {
		String username = "doe";
		
		mockMvc.perform(get("/users/" + username)
	            .contentType(MediaType.APPLICATION_JSON)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isUnauthorized())
	            .andExpect(jsonPath("$.status").value("unauthorized"));
	}
	
	@Test
	@WithMockUser(username = "john", roles={"MEMBER"})
	public void deleteUserUnauthorizedTest() throws Exception {
		String username = "doe";
		
		mockMvc.perform(delete("/users/" + username)
	            .contentType(MediaType.APPLICATION_JSON)
	            .characterEncoding("utf-8"))
				.andExpect(status().isUnauthorized())
		        .andExpect(jsonPath("$.status").value("unauthorized"));
	}
	
	@Test
	@WithMockUser(username = "john", roles={"MEMBER"})
	public void createUserUnauthorizedTest() throws Exception {
		String jsonString = "{ " 
				+ "\"username\": \"newuser\", " 
				+ "\"password\": \"secret\", " 
				+ "\"role\": \"ROLE_MEMBER\", " 
				+ "\"enabled\": true " 
				+ "}";
		
		mockMvc.perform(post("/users")
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(jsonString)
	            .characterEncoding("utf-8"))
				.andExpect(status().isUnauthorized())
		        .andExpect(jsonPath("$.status").value("unauthorized"));
	}
	
	@Test
	@WithMockUser(username = "john", roles={"MEMBER"})
	public void updateUserUnauthorizedTest() throws Exception {
		String username = "john";
		String jsonString = "{ " 
				+ "\"password\": \"secret\" "
				+ "}";
		
		mockMvc.perform(patch("/users/" + username)
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(jsonString)
	            .characterEncoding("utf-8"))
				.andExpect(status().isUnauthorized())
		        .andExpect(jsonPath("$.status").value("unauthorized"));
	}

}
