package posmy.interview.boot.user;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerLibrarianTest {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	@WithMockUser(username = "libadmin", roles={"LIBRARIAN"})
	public void getUserTest() throws Exception {
		String username = "john";
		
		mockMvc.perform(get("/users/" + username)
	            .contentType(MediaType.APPLICATION_JSON)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isOk())
	            .andExpect(jsonPath("$.status").value("success"));
	}
	
	@Test
	@WithMockUser(username = "libadmin", roles={"LIBRARIAN"})
	public void createUserTest() throws Exception {
		String jsonString = "{ " 
				+ "\"username\": \"newuser\", " 
				+ "\"password\": \"secret\", " 
				+ "\"role\": \"ROLE_MEMBER\", " 
				+ "\"enabled\": true " 
				+ "}";
		
		mockMvc.perform(post("/users")
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(jsonString)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isCreated())
	            .andExpect(jsonPath("$.status").value("success"))
	            .andExpect(jsonPath("$.details[0].message").value("User created successfully"));
	}
	
	@Test
	@WithMockUser(username = "libadmin", roles={"LIBRARIAN"})
	public void updateUserTest() throws Exception {
		String username = "john";
		String jsonString = "{ " 
				+ "\"password\": \"secret\" "
				+ "}";
		
		mockMvc.perform(patch("/users/" + username)
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(jsonString)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isOk())
	            .andExpect(jsonPath("$.status").value("success"))
	            .andExpect(jsonPath("$.details[0].message").value("User updated successfully"));
	}
	
	@Test
	@WithMockUser(username = "libadmin", roles={"LIBRARIAN"})
	public void deleteUserTest() throws Exception {
		String username = "john";
		
		mockMvc.perform(delete("/users/" + username)
	            .contentType(MediaType.APPLICATION_JSON)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isNoContent());
	}
	
	@Test
	@WithMockUser(username = "libadmin", roles={"LIBRARIAN"})
	public void createDuplicateUserTest() throws Exception {
		String jsonString = "{ " 
				+ "\"username\": \"john\", " 
				+ "\"password\": \"secret\", " 
				+ "\"role\": \"ROLE_MEMBER\", " 
				+ "\"enabled\": true " 
				+ "}";
		
		mockMvc.perform(post("/users")
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(jsonString)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isBadRequest())
	            .andExpect(jsonPath("$.status").value("failure"))
	            .andExpect(jsonPath("$.details[0].message").value("Username taken"));
	}
	
	@Test
	@WithMockUser(username = "libadmin", roles={"LIBRARIAN"})
	public void getUserNotFoundTest() throws Exception {
		String username = "alien";
		
		mockMvc.perform(get("/users/" + username)
	            .contentType(MediaType.APPLICATION_JSON)
	            .characterEncoding("utf-8"))
	            .andExpect(status().isBadRequest())
	            .andExpect(jsonPath("$.status").value("failure"))
	            .andExpect(jsonPath("$.details[0].message").value("User not found"));
	}

}
